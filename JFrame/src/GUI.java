import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * 
 * @author Sepehr Raissian
 * 
 *
 */
public class GUI extends JFrame implements ActionListener{
	final int SIZE=7;
	JButton jbtCalculate = new JButton("Calculate");
	//score text fields
JTextField [] jtxtScore = new JTextField[SIZE];
JTextField [] jtxtWeight = new JTextField[SIZE];
JTextField jtxtName =  new JTextField(20);
JLabel [] jlblModuleName = new JLabel[SIZE];
JLabel jlblOutput = new JLabel("0");
JPanel scorePanel = new JPanel();
JPanel buttonPanel = new JPanel();


double [] scores = new double[2];//use for scoreCalculator method.
double [] weights = new double[SIZE];//use for scoreCalculator method.


// double [] x = {9.0,10.5};

/**
 * constructor the graphic user interface for score calculator.
 */
public GUI(){
	//score panel (top)
	jlblModuleName[0] = new JLabel("Student Name");
	jlblModuleName[1] = new JLabel("Assigments");
	jlblModuleName[2] = new JLabel("Midterm Exam");
	jlblModuleName[3] = new JLabel("Final Project");
	jlblModuleName[4] = new JLabel("Final Exam");
	jlblModuleName[5] = new JLabel("Grade Average");
	jlblModuleName[6] = new JLabel("Letter Grade");
	
	scorePanel.setBackground(Color.LIGHT_GRAY);
	scorePanel.setLayout(new GridLayout(7,3));
	jbtCalculate.addActionListener(this);
	
	
	for (int i=0; i<SIZE;i++){
		scorePanel.add(jlblModuleName[i]);
		jtxtScore[i] = new JTextField(8);
		scorePanel.add(jtxtScore[i]);
		jtxtWeight[i] = new JTextField(8);
		scorePanel.add(jtxtWeight[i]);
	}
	
	// button panel (bottom)
	buttonPanel.setBackground(Color.darkGray);
	//jbtCalculate.addActionListener(this);
	buttonPanel.add(jbtCalculate);
	add(scorePanel, BorderLayout.CENTER);
	add(buttonPanel, BorderLayout.SOUTH);
	setVisible(true);
	pack();
	setLocationRelativeTo(null);
}
/**
 * calculate grade average scores
 * returns the grade
 * sets to a new score
 * sets to a new weight
 * @param score
 * @param weight
 * @return grade
 */

public double calculateScore(double score, double weight){

	return score*weight;
}
	/**
	 * returns the letter gardes stored in a char values('A','B','C','D','F')
	 * @param score
	 * @return ('A','B','C','D','F')
	 */
public char calculateLetterGrade(double score){
	if(score < 50)
    {
        return 'F';
    }
    else if(score < 65)
    {
        return 'D';
    }
    else if(score < 80)
    {
        return 'C';
    }
    else if(score < 90)
    {
        return 'B';
    }
    else
    {
        return 'A';
    }
        
	
}
@Override
public void actionPerformed(ActionEvent e ) 
{    
    double total = 0;
    for (int i=1; i < 5; i++ ) 
    { 
        total += calculateScore(Double.parseDouble(jtxtScore[i].getText()),Double.parseDouble(jtxtWeight[i].getText()));
    }

    
    jtxtScore[5].setText(total + "");
    jtxtScore[6].setText(calculateLetterGrade(total)+"");
	}

}
